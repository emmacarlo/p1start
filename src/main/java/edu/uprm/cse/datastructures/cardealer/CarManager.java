package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")

public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> list = CarList.getInstance();
	
	//returns an array of type Car containing all the elements in list
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarList() {
		Car[] result = new Car[list.size()];
		if (list.isEmpty()) return result;
		
		for (int i = 0; i < result.length; i++) {
			result[i] = list.get(i);
		}
		return result;
	}
	
	//returns Car matching the id
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getCarId()==id) {
				return (Car)list.get(i);
			}
		}
		throw new NotFoundException();		
	}
	
	//adds a Car to list
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {
		list.add(c);
		return Response.status(201).build();
	}
	
	//updates Car matching the id
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car c) {
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).getCarId() == c.getCarId()) {
				list.remove(i);
				list.add(c);
				return Response.status(200).build();
			}
		}
		return Response.status(404).build();
	}
	
	//deletes Car with matching id from list
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).getCarId() == id) {
				list.remove(i);
				return Response.status(200).build();
			}
		}
		return Response.status(404).build();
	}
	
	
}
