package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car c1, Car c2) {
		//Based on brand, model, and options
		//equals
		if(c1.getCarBrand().equals(c2.getCarBrand()) && 
				c1.getCarModel().equals(c2.getCarModel()) && 
				c1.getCarModelOption().equals(c2.getCarModelOption())) {
			return 0;
		}
		
		//greater than
		else if((c1.getCarBrand().compareTo(c2.getCarBrand()) > 0) &&
				(c1.getCarModel().compareTo(c2.getCarModel()) > 0) &&
				(c1.getCarModelOption().compareTo(c2.getCarModelOption())) >0) {
			return 1;
		}
		
		//less than
		else {
			return -1;
		}
	}

}
